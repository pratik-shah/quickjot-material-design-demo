package is.uncommon.quickjot.fragments;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Fragment;
import android.database.Cursor;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import android.content.Intent;

import org.w3c.dom.Text;

import is.uncommon.quickjot.R;
import is.uncommon.quickjot.activities.DefaultActivity;
import is.uncommon.quickjot.activities.NewNoteActivity;
import is.uncommon.quickjot.adapters.NoteAdapter;
import is.uncommon.quickjot.other.QueryCache;

/**
 * Created by Prats on 18/08/14.
 */
public class ListerFragment extends Fragment implements AdapterView.OnItemClickListener, QueryCache.Callbacks
{

    public interface Callbacks
    {
        public void callBack(Cursor cursor, boolean isStoredNote, View titleView);
    }

    Cursor mCursor = null;
    NoteAdapter mAdapter = null;
    ListView mNoteList = null;
    ViewGroup mContainer = null;
    QueryCache queryObj = null;
    ViewSwitcher switcher = null;
    Activity mActivity = null;
    ListerFragment.Callbacks mActivityCallback = null;
    View vTitle = null;

    public ListerFragment()
    {
        queryObj= new QueryCache(getActivity());
        queryObj.doQuery("SELECT _id, title FROM notes",(QueryCache.Callbacks)this,true,false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(savedInstanceState != null)
        {
            queryObj = new QueryCache(getActivity());
            queryObj.doQuery(savedInstanceState.getString("RestartQuery"),(QueryCache.Callbacks)this,true,false);
        }
        mContainer = container;
        View rootView = inflater.inflate(R.layout.lister_fragment_layout, container, false);
        switcher = (ViewSwitcher)rootView.findViewById(R.id.switcher);
        mNoteList = (ListView)rootView.findViewById(R.id.notesTitleList);
        mNoteList.setOnItemClickListener(this);
        ImageView img = (ImageView) rootView.findViewById(R.id.loaderContainer);
        img.setBackgroundResource(R.drawable.animation);
        AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();
        frameAnimation.start();
        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        NoteAdapter adpter = (NoteAdapter)adapterView.getAdapter();
        Cursor c = adpter.getCursor();
        c.moveToPosition(i);
        String title = c.getString(c.getColumnIndex(c.getColumnName(1)));
        Log.d("titleis", title);
        vTitle = view.findViewById(R.id.noteTitle);
        String query = "SELECT _id, title, content FROM notes where title = '" +title+"';";
        queryObj.doQuery(query,(QueryCache.Callbacks)this,true,true);
    }

    @Override
    public void onQueryResult(Cursor cursor, boolean isStoredNote)
    {
        mCursor = cursor;
        if(!isStoredNote)
        {
            switcher.setDisplayedChild(1);
            if(mAdapter == null)
            {
                mAdapter = new NoteAdapter(getActivity(), mCursor);
                mNoteList.setAdapter(mAdapter);
            }
            else
                mAdapter.changeCursor(mCursor);
        }
        else
        {
            mActivityCallback.callBack(mCursor, isStoredNote, vTitle);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        if(queryObj != null)
        {
            String restartQuery = queryObj.clearQuery();
            outState.putString("RestartQuery", restartQuery);
        }
    }

    public void handleResume()
    {
        queryObj.doQuery("SELECT _id, title FROM notes",(QueryCache.Callbacks)this,true,false);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(mCursor != null)
            mCursor.close();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        mActivity = activity;
        if(activity instanceof ListerFragment.Callbacks)
        {
            mActivityCallback = (ListerFragment.Callbacks)activity;
        }
    }
}
