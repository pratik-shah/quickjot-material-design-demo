package is.uncommon.quickjot.activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Outline;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.ChangeBounds;
import android.transition.MoveImage;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.TextView;

import is.uncommon.quickjot.R;
import is.uncommon.quickjot.fragments.ListerFragment;
import is.uncommon.quickjot.helpers.NoteDBHelper;



public class DefaultActivity extends Activity implements View.OnClickListener, ListerFragment.Callbacks
{
    Button mNewNote = null;
    ListerFragment mListerFragment = null;
    View mCardView = null;
    Animation trans = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        setContentView(R.layout.defualt_activity_layout);

        Transition transition = new ChangeBounds();
        transition.setInterpolator(new AccelerateDecelerateInterpolator());

        getWindow().setSharedElementEnterTransition(transition);
        getWindow().setSharedElementExitTransition(transition);

        mNewNote = (Button) this.findViewById(R.id.newNoteButton);
        mNewNote.setOnClickListener(this);
        Outline mOutlineCircle;
        int shapeSize = getResources().getDimensionPixelSize(R.dimen.shape_size);
        mOutlineCircle = new Outline();
        mOutlineCircle.setRoundRect(0, 0, shapeSize, shapeSize, shapeSize / 2);
        mNewNote.setOutline(mOutlineCircle);
        mNewNote.setClipToOutline(true);
        mCardView = (View)this.findViewById(R.id.topLayout);
        super.onResume();
        trans = new TranslateAnimation(800, 0, 0, 0);
        trans.setInterpolator(new AccelerateInterpolator());
        trans.setDuration(800);
        mNewNote.setAnimation(trans);
        mCardView.setAnimation(trans);
        trans.start();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        SQLiteDatabase mDatabase = NoteDBHelper.getInstance(this).getWritableDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM notes", null);
        if(cursor.getCount() == 0)
        {
            Log.d("onResume()", "cursor empty");
            mListerFragment = null;
            Log.d("onResume()", "cursor empty: replacing with dummy fragment");
            getFragmentManager().beginTransaction().replace(R.id.fragment_container, new DummyFragment()).commit();
        }
        else
        {
            Log.d("onResume", "cursor not empty");
            if(mListerFragment == null)
            {
                Log.d("onResume", "Lister Fragment object is null");
                mListerFragment = new ListerFragment();
                Log.d("onResume", "cursor not empty: replacing with lister fragment");
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, mListerFragment).commit();
            }
            else
            {
                Log.d("onResume", "ListerFragment Available: Reloading Data with handleResume()");
                mListerFragment.handleResume();
            }
        }
        cursor.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.defualt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_settings)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {
        Intent intent = new Intent(this, NewNoteActivity.class);
        startActivity(intent);
    }

    @Override
    public void callBack(Cursor cursor, boolean isStoredNote, View titleView)
    {
        Intent intent = new Intent(this, NewNoteActivity.class);
        cursor.moveToFirst();
        titleView.setViewName("Title");
        TextView t = (TextView)titleView;
        Log.d("Clicked View Text", t.getText().toString());
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, titleView, "Title");
        intent.putExtra("isStoredNote", isStoredNote);
        intent.putExtra("title",cursor.getString(1));
        intent.putExtra("content",cursor.getString(2));
        startActivity(intent, options.toBundle());
    }


    public static class DummyFragment extends Fragment
    {
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View rootView = inflater.inflate(R.layout.default_no_note_fragment_layout, container, false);
            return rootView;
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }
}
