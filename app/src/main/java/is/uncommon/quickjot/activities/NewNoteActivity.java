package is.uncommon.quickjot.activities;

import android.app.Activity;

import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.ChangeBounds;
import android.transition.ChangeTransform;
import android.transition.MoveImage;
import android.transition.Slide;
import android.transition.Transition;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.app.AlertDialog;
import android.view.animation.Animation;

import android.content.DialogInterface;
import android.widget.Toast;

import is.uncommon.quickjot.R;
import is.uncommon.quickjot.other.QueryCache;

/**
 * Created by Prats on 12/08/14.
 */
public class NewNoteActivity extends Activity implements View.OnClickListener
{
    Button mSaveButton = null;
    Button mRemindMeButton = null;
    Button mDeleteButton = null;
    EditText mTitleNote = null;
    EditText mContentNote = null;
    AlertDialog alert11 = null;
    boolean mStoredNote = false;
    String mTitle = null;
    Animation trans1 = null;
    Animation trans2 = null;
    Animation trans3 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        setContentView(R.layout.note_content_layout);

        Transition transition = new ChangeBounds();
        transition.setInterpolator(new AccelerateDecelerateInterpolator());
        getWindow().setSharedElementEnterTransition(transition);
        getWindow().setSharedElementExitTransition(transition);

        mSaveButton = (Button)this.findViewById(R.id.saveButton);
        mSaveButton.setOnClickListener(this);
        mRemindMeButton = (Button)this.findViewById(R.id.remindMeButton);
        mRemindMeButton.setOnClickListener(this);
        mDeleteButton = (Button)this.findViewById(R.id.deleteButton);
        mDeleteButton.setOnClickListener(this);
        mTitleNote = (EditText)this.findViewById(R.id.newNoteTitle);
        mTitleNote.setViewName("Title");
        mContentNote = (EditText)this.findViewById(R.id.newNoteContent);
        if(getIntent().getBooleanExtra("isStoredNote",false))
        {
            mStoredNote = true;
            mTitle = getIntent().getStringExtra("title");
            mTitleNote.setText(getIntent().getStringExtra("title"));
            mContentNote.setText(getIntent().getStringExtra("content"));
        }
        else
            mDeleteButton.setText("CANCEL");


        trans1 = new TranslateAnimation(800, 0, 0, 0);
        trans2 = new TranslateAnimation(800, 0, 0, 0);
        trans3 = new TranslateAnimation(800, 0, 0, 0);
        trans1.setInterpolator(new AccelerateInterpolator());
        trans2.setInterpolator(new AccelerateInterpolator());
        trans3.setInterpolator(new AccelerateInterpolator());
        trans1.setDuration(1000);
        trans2.setDuration(1400);
        trans3.setDuration(1800);
        mSaveButton.setAnimation(trans1);
        mRemindMeButton.setAnimation(trans2);
        mDeleteButton.setAnimation(trans3);
        trans1.start();
        trans2.start();
        trans3.start();
    }


    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.saveButton:
                if(mTitleNote.getText().toString().matches(""))
                {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                    builder1.setTitle("Error");
                    builder1.setMessage("Title Can't Be Empty!");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    alert11 = builder1.create();
                    alert11.show();
                    break;
                }
                else
                {
                    QueryCache queryCache = new QueryCache(this);
                    String query = null;
                    if(mStoredNote)
                    {
                        query = "UPDATE notes SET title = '"+ mTitleNote.getText().toString() + "', content = '" + mContentNote.getText().toString() + "' WHERE title = '" + mTitle + "';";
                    }
                    else
                    {
                        query = "INSERT INTO notes(title,content) VALUES('" + mTitleNote.getText().toString() + "', '" + mContentNote.getText().toString() + "');";
                    }
                    queryCache.doQuery(query,null,false,false);
                    Toast.makeText(this, "Note was saved!", Toast.LENGTH_LONG).show();
                }
            case R.id.deleteButton:
                if(mStoredNote)
                {
                    QueryCache queryCache = new QueryCache(this);
                    String query = "DELETE FROM notes WHERE title = '" + mTitle + "';";
                    queryCache.doQuery(query,null,false,false);
                    Toast.makeText(this,"Note was deleted!", Toast.LENGTH_LONG).show();
                }
                finish();
                break;
            case R.id.remindMeButton:
                break;
        }

    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if(alert11 != null)
            alert11.dismiss();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

    }
}

