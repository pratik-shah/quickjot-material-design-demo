package is.uncommon.quickjot.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Outline;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.CursorAdapter;
import android.widget.TextView;
import java.util.Random;

import is.uncommon.quickjot.R;

/**
 * Created by Prats on 18/08/14.
 */
public class NoteAdapter extends CursorAdapter
{
    long time = 1000;
    public NoteAdapter(Context context, Cursor c)
    {
        super(context,c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        View rootView = inflater.inflate(R.layout.row, viewGroup, false);
        Animation translation = new TranslateAnimation(800, 0, 0, 0);
        translation.setInterpolator(new AccelerateInterpolator());
        int position = cursor.getPosition();
        translation.setDuration((time + (50*position)));
        rootView.setAnimation(translation);
        translation.start();
        return rootView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        TextView noteTitle = (TextView)view.findViewById(R.id.noteTitle);
        TextView reminderStatus = (TextView)view.findViewById(R.id.reminderStatus);
        TextView initialTitle = (TextView)view.findViewById(R.id.initialsTitle);

        noteTitle.setText(cursor.getString(cursor.getColumnIndexOrThrow(cursor.getColumnName(1))));
        reminderStatus.setText("This note isn't a reminder type!");
        char c = noteTitle.getText().toString().charAt(0);
        initialTitle.setText(String.valueOf(c));
        Outline mOutlineCircle;
        int shapeSize = context.getResources().getDimensionPixelSize(R.dimen.initial_size);
        mOutlineCircle = new Outline();
        mOutlineCircle.setRoundRect(0, 0, shapeSize, shapeSize, shapeSize / 2);
        initialTitle.setOutline(mOutlineCircle);
        initialTitle.setClipToOutline(true);
        Random random = new Random();
        int r = random.nextInt(256);
        int g = random.nextInt(256);
        int b = random.nextInt(256);
        initialTitle.setBackgroundColor(Color.argb(200, r,g, b));
    }
}
