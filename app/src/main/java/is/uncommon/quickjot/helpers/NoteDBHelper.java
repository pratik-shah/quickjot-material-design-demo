package is.uncommon.quickjot.helpers;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.util.Log;

/**
 * Created by Prats on 12/08/14.
 */
public class NoteDBHelper extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME = "notes.db";
    private static final int DB_SCHEMA = 1;
    private static NoteDBHelper sInstance = null;

    public NoteDBHelper(Context context)
    {
        super(context,DATABASE_NAME,null,DB_SCHEMA);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        Log.d("onCreate() DBHelper", "Database Object Used: " + Integer.toString(System.identityHashCode(sqLiteDatabase)));
        sqLiteDatabase.execSQL("CREATE TABLE notes (_id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, content TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2)
    {

    }

    public static final NoteDBHelper getInstance(Context context)
    {
        if(sInstance == null)
        {
            sInstance = new NoteDBHelper(context.getApplicationContext());
        }
        Log.d("getInstance()", "Database Object Returned Is: " + Integer.toString(System.identityHashCode(sInstance)));
        return sInstance;
    }
}
