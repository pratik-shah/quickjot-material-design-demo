package is.uncommon.quickjot.other;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import is.uncommon.quickjot.helpers.NoteDBHelper;
import is.uncommon.quickjot.service.DBInterfacerService;

/**
 * Created by Prats on 14/08/14.
 */
public class QueryCache
{

    public interface Callbacks{
      public void onQueryResult(Cursor cursor, boolean isStoredNote);
    };

    private static final String ACTION_QUERY = "is.uncommon.quickjot.querycache";
    Context mActivityContext = null;
    DBSignalReceiver receiver = null;
    Cursor cursor = null;
    String mQuery = null;
    QueryTask mTask = null;
    boolean mStoredNote = false;

    public QueryCache(Context context)
    {
        mActivityContext = context;
    }

    public void doQuery(String query, Callbacks callbacks, boolean isResultList, boolean isStoredNote)
    {
        mStoredNote = isStoredNote;
        mQuery = query;
        Log.d("doQuery()", "Query Is: " + mQuery);
        if(!isResultList)
        {
            receiver = new DBSignalReceiver();
            IntentFilter filter = new IntentFilter(ACTION_QUERY);
            LocalBroadcastManager.getInstance(mActivityContext).registerReceiver(receiver,filter);
            Intent intent = new Intent(mActivityContext, DBInterfacerService.class);
            intent.putExtra("Query", query);
            mActivityContext.startService(intent);
        }
        else
        {
           mTask = new QueryTask(callbacks);
           mTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);
        }

    }

    public String clearQuery()
    {
        if(mTask != null)
            mTask.cancel(true);
        return mQuery;
    }

    public class DBSignalReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent)
        {
            LocalBroadcastManager.getInstance(mActivityContext).unregisterReceiver(receiver);
        }
    }

    public class QueryTask extends AsyncTask<String,Void,Cursor>
    {
        Callbacks call = null;

        public QueryTask(Callbacks callbacks)
        {
            call = callbacks;
        }

        @Override
        protected Cursor doInBackground(String... strings)
        {
            Log.d("doInBackground()", "Query Received Is: " + strings[0]);
            SQLiteDatabase db = NoteDBHelper.getInstance(mActivityContext).getWritableDatabase();
            Log.d("doInBackground()", "Database Object used is: " + Integer.toString(System.identityHashCode(db)));
            Cursor cursor = db.rawQuery(strings[0],null);
            return cursor;
        }

        @Override
        protected void onPostExecute(Cursor cursor)
        {
            call.onQueryResult(cursor,mStoredNote);
        }
    }
}
