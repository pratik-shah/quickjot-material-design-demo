package is.uncommon.quickjot.service;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import is.uncommon.quickjot.helpers.NoteDBHelper;
import is.uncommon.quickjot.other.QueryCache;

/**
 * Created by Prats on 14/08/14.
 */
public class DBInterfacerService extends IntentService
{
    public DBInterfacerService(String name)
    {
        super(name);
    }

    public DBInterfacerService()
    {
        this("DBInterfacerService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        String query = intent.getStringExtra("Query");
        Log.d("onHandleIntentService", "Query to be run: "+ query);
        SQLiteDatabase database = NoteDBHelper.getInstance(this).getWritableDatabase();
        Log.d("onHandleIntentService", "Database Object Used: " + Integer.toString(System.identityHashCode(database)));
        database.execSQL(query);
        Intent bIntent = new Intent(this, QueryCache.class);
        sendBroadcast(bIntent);
        stopSelf();
    }
}
